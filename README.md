# Wacom Cintiq Pro Linux

Here are some notes on how to get the Cintiq Pro work on Debian/Ubuntu (22.04).

It can be a good idea to install some Wacom packages:
```
sudo apt install libwacom-bin xserver-xorg-input-wacom ddcutil
```


## Set maximum brightness

To set maximum brightness, run:
```
src/set_max_brightness.sh
```
(install requirements first, `apt install ddcutil`).

You can tweak a couple of other Wacom settings too, using
`ddcutil`. Here is how to list all settings:
```
MFG_ID=$(sudo ddcutil detect | grep -B 1 Cintiq | head -n 1 | awk '{print $3}')
sudo ddcutil --mfg=$MFG_ID capabilities
```

You can then tweak a specific setting:
```
sudo ddcutil --mfg=$MFG_ID setvcp $SETTING 100
```


## Calibrate stylus to the Wacom resolution

The mouse pointer may not follow your Stylus point correctly out of the box.

First review `src/calibrate_reso.sh` and check:

 - if you need to change the output settings at the top of the script
to match your monitor outputs (to see list of monitor outputs, run
`xrandr | grep connected`).

 - the review the `xrandr` calls, to use your desired resolutions

Then run:
```
src/calibrate_reso.sh
```


That script was copied from Joakim Verona, somewhere in his repos at
https://gitlab.com/jave - I can't seem to find it right now.


## Window management in i3

To have Krita etc open dialogs on the Wacom when using [i3 wm](https://i3wm.org/),
simply make sure you start Krita from the Wacom virtual desktop
(i.e. start a terminal on the Wacom, and execute Krita in it).


## Configure stylus pressure

Connect and power up your your Wacom, then run:
```
gnome-control-center wacom
```


