#!/bin/sh
#
# Script copied from Joakim Verona, from one of his repos at https://gitlab.com/jave
# - can't find which, sorry.
#


# eDP-1 is internal screen on dell precision
# DP-1 is wacom screen, or HDMI-1, if through adapter
INTERNALOUT=eDP-1

# with wacom on usb-c
WACOMOUT=DP-1

# wacom on usb-c, nvidia drivers, needs a separate setting for xsetting, dp-1 does not work, use head-1
# with nouveau drivers use dp-1
#TWACOMOUT=HEAD-1
TWACOMOUT=DP-1


function xs {
    xsetwacom set "Wacom Cintiq Pro 13 Touch Finger touch" MapToOutput ${TWACOMOUT}
    xsetwacom set "Wacom Cintiq Pro 13 Pen stylus" MapToOutput ${TWACOMOUT}
    xsetwacom set "Wacom Cintiq Pro 13 Pen eraser" MapToOutput ${TWACOMOUT}
    xsetwacom set "Wacom Cintiq Pro 13 Pad pad" MapToOutput ${TWACOMOUT}

}

function writestate {
    echo $1 > ~/.mywacomstate
}

previousstate=$(cat ~/.mywacomstate)

echo "previousstate:" $previousstate
function restart {
    # there seems to be a timing issue or something here
    sleep 5
    i3-msg restart


}
if [ $# == 0  ]
then
    # im not using this "next state" feature atm
    #nextstate=$(expr \( $previousstate + 1 \) % 3)
    nextstate=0
    echo "next: $previousstate"
else
    nextstate=$1
fi

#xrandr --output eDP-1 --primary --mode 3840x2160 --pos 0x0 --rotate normal --output DP-1 --mode 1920x1080 --pos 3840x0 --rotate normal --output HDMI-1 --off --output DP-2 --off --output HDMI-2 --off
if [ $nextstate == "0"  ]
then
    # both screens
    # IF gnome flasback is working, then you shouldnt need the below workaround
    xrandr --output ${INTERNALOUT} --primary --mode 1920x1080 --scale 1x1 --pos 0x0 --rotate normal --output ${WACOMOUT}  --scale 1x1 --right-of ${INTERNALOUT} --mode 1920x1080 --pos 1920x0 --rotate normal ${DISABLESCREENS}
    restart
    xs
    writestate 0
elif [ $nextstate == "1"  ]
then     
    # only wacom
    xrandr --output ${INTERNALOUT} --off  --output ${WACOMOUT} --scale 1x1 --primary --mode 1920x1080 --pos 0x0 --rotate normal ${DISABLESCREENS}
    restart
    xs
    writestate 1
elif [ $nextstate == "2"  ]
then     
    # only laptop
    xrandr --output ${INTERNALOUT} --primary --mode 1920x1080 --scale 1x1 --pos 0x0 --rotate normal  --off  --output ${WACOMOUT} --off  ${DISABLESCREENS}
    restart
    xs
    writestate 2
elif [ $1 == "x"  ]
then     
    xs
fi    
