#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

#sudo apt install ddcutil
sudo modprobe i2c-dev

MFG_ID=$(sudo ddcutil detect | grep -B 1 Cintiq | head -n 1 | awk '{print $3}')

REG_BRIGHTNESS=$(sudo ddcutil --mfg=$MFG_ID capabilities | grep -i '(Brightness)' | awk '{print $2}')

# show current brightness
sudo ddcutil --mfg=$MFG_ID getvcp $REG_BRIGHTNESS

# set to full brightness
sudo ddcutil --mfg=$MFG_ID setvcp $REG_BRIGHTNESS 100
